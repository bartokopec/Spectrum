# Sexuality Spectrum

 - [Website](https://spectrum.avris.it)

## Local copy

    make install
    make run
    
## Copyright
 
 * **Author:** Andrea Prusinowski [(Avris.it)](https://avris.it)
 * **Licence:** [MIT](https://mit.avris.it)
